package onixnet.com.sdk;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import onixnet.com.sdk.model.sharing_folders.SharingFolder;
import onixnet.com.sdk.utils.Utils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

public class BroadcomSharingFolder extends BroadcomResource {

    public BroadcomSharingFolder(BroadcomAPIConnection broadcomAPIConnection) {
        super(broadcomAPIConnection);
    }

    public SharingFolder getSharingFolder(String folderId) {
        BroadcomAPIRequest request = new BroadcomAPIRequest(this.getHttpclient(),
            this.getBroadcomHostUrl() + "/sharing-folders/" + folderId, "GET");
        CloseableHttpResponse response = request.execute();
        Gson gson = new Gson();
        try {
            return gson.fromJson(EntityUtils.toString(response.getEntity()), SharingFolder.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<SharingFolder> getAllSharingFolders() {
        try {
            BroadcomAPIRequest request = new BroadcomAPIRequest(this.getHttpclient(),
                this.getBroadcomHostUrl() + "/sharing-folders", "GET");
            CloseableHttpResponse response = request.execute();
            List<SharingFolder> folders = Utils.convertJsonToPOJO(response, SharingFolder.class);
            return folders;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
