package onixnet.com.sdk;

import java.io.IOException;
import java.util.logging.Logger;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

public class BroadcomAPIRequest {

    private static final Logger LOGGER = Logger.getLogger(BroadcomAPIRequest.class.getName());
    private final CloseableHttpClient httpClient;
    private final String urlRequest;
    private final String method;

    public BroadcomAPIRequest(CloseableHttpClient httpClient, String urlRequest, String method) {
        this.httpClient = httpClient;
        this.urlRequest = urlRequest;
        this.method = method;
    }

    public CloseableHttpResponse execute() {
        HttpGet httpget = new HttpGet(urlRequest);
        httpget.setHeader("Accept", "application/json");
        System.out.println("Executing request " + httpget.getRequestLine());
        try {
            CloseableHttpResponse response = this.httpClient.execute(httpget);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
