package onixnet.com.sdk.utils;

import com.google.gson.Gson;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import onixnet.com.sdk.model.sharing_folders.SharingFolder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

public class Utils {

    public static List<SharingFolder> convertJsonToPOJO(CloseableHttpResponse response,
        Type target)
        throws IOException {
        String responseBody = EntityUtils.toString(response.getEntity());
        JSONArray myObject = new JSONArray(responseBody);
        Iterator iterator = myObject.iterator();
        Gson gson = new Gson();
        List<SharingFolder> foldersList = new ArrayList<>();
        while (iterator.hasNext()) {
            String sharingFolder = iterator.next().toString();
            SharingFolder object = gson.fromJson(sharingFolder, target);
            foldersList.add(object);
        }
        return foldersList;
    }
}
