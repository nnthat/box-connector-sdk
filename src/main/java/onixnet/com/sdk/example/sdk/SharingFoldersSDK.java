package onixnet.com.sdk.example.sdk;

import java.util.Iterator;
import java.util.List;
import onixnet.com.sdk.BroadcomAPIConnection;
import onixnet.com.sdk.BroadcomSharingFolder;
import onixnet.com.sdk.model.sharing_folders.SharingFolder;

public class SharingFoldersSDK {

    private static final String SERVICE_BROADCOM_HOST_NAME = "192.168.92.192:9080";
    private static final String SERVICE_USER_NAME = "admin";
    private static final String SERVICE_PASSWORD = "admin";
    private static SharingFoldersSDK serviceBroadcomAPI;
    private static BroadcomAPIConnection broadcomAPIConnection;

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        serviceBroadcomAPI = new SharingFoldersSDK();
        broadcomAPIConnection = new BroadcomAPIConnection(SERVICE_BROADCOM_HOST_NAME,
            SERVICE_USER_NAME, SERVICE_PASSWORD);
        serviceBroadcomAPI.getAllSharingFolders();
        System.out.println("Completed in: " + (System.currentTimeMillis() - startTime) / 1000);
    }

    private void getAllSharingFolders() {
        BroadcomSharingFolder sharingFolder = new BroadcomSharingFolder(broadcomAPIConnection);
        List<SharingFolder> folders = sharingFolder.getAllSharingFolders();
        Iterator iterator = folders.iterator();
        SharingFolder folder = null;
        while (iterator.hasNext()) {
            folder = (SharingFolder) iterator.next();
            System.out.println("Folder id: " + folder.getId() + ", owner: " + folder.getOwner());
        }
    }
}
