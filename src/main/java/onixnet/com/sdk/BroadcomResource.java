package onixnet.com.sdk;

import org.apache.http.impl.client.CloseableHttpClient;

public abstract class BroadcomResource {

    private final BroadcomAPIConnection broadcomAPIConnection;

    public BroadcomResource(BroadcomAPIConnection broadcomAPIConnection) {
        this.broadcomAPIConnection = broadcomAPIConnection;
    }

    public BroadcomAPIConnection getBroadcomAPIConnection() {
        return broadcomAPIConnection;
    }

    public CloseableHttpClient getHttpclient() {
        return broadcomAPIConnection.getHttpClient();
    }

    public String getBroadcomHostUrl() {
        return "http://" + broadcomAPIConnection.getBroadcomHost() + "/api/v1";
    }
}
