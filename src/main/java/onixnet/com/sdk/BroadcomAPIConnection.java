package onixnet.com.sdk;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class BroadcomAPIConnection {

    private String broadcomHost;
    private String broadcomUsername;
    private String broadcomPassword;
    private CloseableHttpClient httpClient;

    public BroadcomAPIConnection(String broadcomHost, String broadcomUsername,
        String broadcomPassword) {
        this.broadcomHost = broadcomHost;
        this.broadcomUsername = broadcomUsername;
        this.broadcomPassword = broadcomPassword;
        this.httpClient = this.authenticate();
    }

    public BroadcomAPIConnection(String broadcomHost, String broadcomUsername,
        String broadcomPassword, CloseableHttpClient httpClient) {
        this.broadcomHost = broadcomHost;
        this.broadcomUsername = broadcomUsername;
        this.broadcomPassword = broadcomPassword;
        this.httpClient = httpClient;
    }

    public String getBroadcomHost() {
        return broadcomHost;
    }

    public void setBroadcomHost(String broadcomHost) {
        this.broadcomHost = broadcomHost;
    }

    public String getBroadcomUsername() {
        return broadcomUsername;
    }

    public void setBroadcomUsername(String broadcomUsername) {
        this.broadcomUsername = broadcomUsername;
    }

    public String getBroadcomPassword() {
        return broadcomPassword;
    }

    public void setBroadcomPassword(String broadcomPassword) {
        this.broadcomPassword = broadcomPassword;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public CloseableHttpClient authenticate() {
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
            new AuthScope(new HttpHost(this.broadcomHost)),
            new UsernamePasswordCredentials(this.broadcomUsername, this.broadcomPassword));
        return HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
    }
}
